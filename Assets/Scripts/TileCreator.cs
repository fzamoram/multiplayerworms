using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileCreator : MonoBehaviour
{
    public Tilemap tilemap;
    public TileBase tile;
    private bool isDrawing = false;

    public TilemapData tilemapData;


    private void Awake()
    {
        tilemapData.tilePositions = new Vector3Int[0];
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            isDrawing = true;
        }

        if (Input.GetMouseButtonUp(1))
        {
            isDrawing = false;

            // Guarda la información del Tilemap en el ScriptableObject
            SaveTilemapData();
        }

        if (isDrawing)
        {
            Vector3Int cellPosition = tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            DrawCube(cellPosition);
        }
    }

    void DrawCube(Vector3Int center)
    {
        int size = 3;

        for (int x = center.x - size; x <= center.x + size; x++)
        {
            for (int y = center.y - size; y <= center.y + size; y++)
            {
                for (int z = center.z - size; z <= center.z + size; z++)
                {
                    Vector3Int position = new Vector3Int(x, y, z);
                    tilemap.SetTile(position, tile);
                }
            }
        }
    }

    void SaveTilemapData()
    {
        BoundsInt bounds = tilemap.cellBounds;

        List<Vector3Int> tilePositions = new List<Vector3Int>();

        for (int x = 0; x < bounds.size.x; x++)
        {
            for (int y = 0; y < bounds.size.y; y++)
            {
                for (int z = 0; z < bounds.size.z; z++)
                {
                    Vector3Int position = new Vector3Int(bounds.x + x, bounds.y + y, bounds.z + z);
                    if (tilemap.GetTile(position) != null)
                    {
                        tilePositions.Add(position);
                    }
                }
            }
        }

        tilemapData.tilePositions = tilePositions.ToArray();
    }
}
