using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
public class Health_Controller : MonoBehaviour
{

    public Image barra_Vida;
    [SerializeField]
    private GameObject worm;
    private float health;
    private float max_Health;

    public void UpdateVida()
    {
        max_Health = worm.GetComponent<Worm_Controller_States>().max_Health;
        health = worm.GetComponent<Worm_Controller_States>().health;
        barra_Vida.fillAmount = health/ max_Health;   
    }


    public void UpdateShield()
    {
        max_Health = worm.GetComponent<Worm_Controller_States>().max_Shield;
        health = worm.GetComponent<Worm_Controller_States>().shield;
        barra_Vida.fillAmount = health / max_Health;
    }
}
