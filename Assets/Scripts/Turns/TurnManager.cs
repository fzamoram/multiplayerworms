using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using Unity.VisualScripting.Dependencies.Sqlite;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
   
    public GameObject teamA;

    public GameObject teamB;

    [SerializeField]
    List<GameObject> listA;
    [SerializeField]
    List<GameObject> listB;

    [SerializeField]
    GameEventSeconds m_GameEvent;

    private int turn = 0;
    private bool endTurn = false;
    private bool shoot = false;
    private int seconds = 0;
    private int turnLength = 20;
    void Start()
    {
        turn = 0;
        Init();
        seconds = turnLength;
        StartCoroutine(UISeconds());
    }
    private void Init()
    {
        for (int i = 0; i < teamA.transform.childCount; i++)
        {
            Transform worm = teamA.transform.GetChild(i);
            listA.Add(worm.gameObject);
            worm.GetComponent<Worm_Controller_States>().TerminaTurno();
        }
        for (int i = 0; i < teamB.transform.childCount; i++)
        {
            Transform worm = teamB.transform.GetChild(i);
            listB.Add(worm.gameObject);
            worm.GetComponent<Worm_Controller_States>().TerminaTurno();
        }
        Debug.Log(teamA.transform.childCount);
        Debug.Log(teamB.transform.childCount);
    }

    public void Load()
    {
        listA = new List<GameObject>();
        listB = new List<GameObject>();
        for (int i = 0; i < teamA.transform.childCount; i++)
        {
            Transform worm = teamA.transform.GetChild(i);
            listA.Add(worm.gameObject);
        }
        for (int i = 0; i < teamB.transform.childCount; i++)
        {
            Transform worm = teamB.transform.GetChild(i);
            listB.Add(worm.gameObject);
        }
    }
    void ChangeTurn()
    {
        turn = (turn + 1) % 2;
        Debug.Log("Turn: " + turn);
        seconds = turnLength;
        GameObject wormActual;
        if (turn == 0)
        {
            listA.Add(listA[0]);
            listA.RemoveAt(0);
            wormActual = listB[0];
        }
        else
        {
            listB.Add(listB[0]);
            listB.RemoveAt(0);
            wormActual = listA[0];
        }

        wormActual.GetComponent<Worm_Controller_States>().TerminaTurno();

        if (turn == 0 && listA.Count > 0)
        {
            if (listA[0] == null)
            {
                return;
            }
            wormActual = listA[0];            
        }
        else if (turn == 1 && listB.Count > 0)
        {
            if (listB[0] == null)
            {
                return;
            }

            wormActual = listB[0];
        }

        wormActual.GetComponent<Worm_Controller_States>().EmpiezaTurno();
        /*
        listB[0].GetComponentInChildren<Arma_Controller>().gameObject.SetActive(true);
        foreach (GameObject t in listA)
        {
            if (t != null)
            {
                t.GetComponent<Worm_Controller_States>().mover = false;
                t.GetComponentInChildren<Arma_Controller>().gameObject.SetActive(false);
            }

        }*/
    }



    private IEnumerator UISeconds()
    {
        ChangeTurn();
        while (listA.Count != 0 && listB.Count != 0)
        {
            UpdateSeconds();
            yield return new WaitForSeconds(1);
        }
    }

    private void UpdateSeconds()
    {
        if (seconds != 0)
        seconds--;

        if (shoot)
        {
            if (endTurn)
            {
                seconds = 0;
                endTurn = false;
                shoot = false;
                ChangeTurn();
            }
        }
        if (seconds == 0 && !shoot)
        {
            ChangeTurn();
        }
        m_GameEvent.Raise(seconds);

        
    }
    public void EndTurn()
    {
        endTurn = true;
    }

    public void Shoot()
    {
        shoot = true;
    }
    public void RemoveWorm(Worm_Controller_States wormScript)
    {
        GameObject worm = wormScript.gameObject;
        
        if (listA.Find(x => x.gameObject == worm) != null)
        {
            listA.Remove(worm);
        }

        if (listB.Find(x => x.gameObject == worm) != null)
        {
            listB.Remove(worm);
        }
        if (listA.Count == 0)
        {
            Debug.Log("Team B Wins");
            return;
        }
            
        if (listB.Count == 0)
        {
            Debug.Log("Team A Wins");
            return;
        }
            

        if (worm.GetComponent<Worm_Controller_States>().mover)
        {
            ChangeTurn();
        }
        
        
    }
}
