using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindeTurnoMisil : MonoBehaviour
{
    [SerializeField]
    private GameEvent m_Event;

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Limit")
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Suelo")
        {
            Destroy(gameObject);
        }
    }
    private void OnDestroy()
    {
        m_Event.Raise();
    }
}
