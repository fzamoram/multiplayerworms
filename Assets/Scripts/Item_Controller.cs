using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Controller : MonoBehaviour
{
    public List<Consumables_SO> consumables;
    public Dictionary<Consumables_SO, int> numItems = new Dictionary<Consumables_SO, int>();
    
    public void Start()
    {
        foreach (Consumables_SO consumable in consumables)
        {
            numItems.Add(consumable,0);
        }

        foreach (var item in numItems)
        {
            print(item.Key);

        }
    }

    /*
    public void PickConsumable(Consumables_SO consumable)
    {
        foreach (var item in numItems)
        {
            print(item.Key);
            if (item.Key == consumable)
            {
                IncrementarValorSiExiste(numItems,consumable);
            }
  
        }
    }

    static void IncrementarValorSiExiste(Dictionary<Consumables_SO, int> diccionario, Consumables_SO clave)
    {
        if (diccionario.ContainsKey(clave))
        {
            diccionario[clave]++;
        }
    }

    */
}
