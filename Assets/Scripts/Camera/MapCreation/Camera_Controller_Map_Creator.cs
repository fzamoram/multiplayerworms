using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller_Map_Creator : MonoBehaviour
{


    public float dragSpeed = 4.0f;
    public float smoothTime = 0.1f;
    public float zoomSpeed = 5.0f;  // Ajusta la velocidad de zoom seg�n sea necesario
    public float minZoom = 5.0f;    // Ajusta el valor m�nimo de zoom seg�n sea necesario
    public float maxZoom = 15.0f;   // Ajusta el valor m�ximo de zoom seg�n sea necesario

    private Vector3 velocity = Vector3.zero;

    void Update()
    {

 
            float zoomInput = Input.GetAxis("Mouse ScrollWheel");
            float newSize = Camera.main.orthographicSize - zoomInput * zoomSpeed * Time.deltaTime;
            newSize = Mathf.Clamp(newSize, minZoom, maxZoom);
            Camera.main.orthographicSize = newSize;
       
    }
}
