using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller : MonoBehaviour
{
    private Vector3 dragOrigin;
    private Vector3 offset;
    private Vector3 lastMousePosition;

    public float dragSpeed = 4.0f;
    public float smoothTime = 0.1f;
    public float zoomSpeed = 5.0f;  // Ajusta la velocidad de zoom seg�n sea necesario
    public float minZoom = 5.0f;    // Ajusta el valor m�nimo de zoom seg�n sea necesario
    public float maxZoom = 15.0f;   // Ajusta el valor m�ximo de zoom seg�n sea necesario

    private Vector3 velocity = Vector3.zero;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            offset = transform.position - dragOrigin;
            lastMousePosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 currentMousePosition = Input.mousePosition;
            Vector3 mouseDelta = currentMousePosition - lastMousePosition;

            Vector3 mouseDirection = Camera.main.ScreenToWorldPoint(currentMousePosition) - Camera.main.ScreenToWorldPoint(lastMousePosition);

            transform.Translate(-mouseDirection * dragSpeed * Time.deltaTime, Space.World);
        }

        lastMousePosition = Input.mousePosition;

        // Zoom usando la posici�n del rat�n
        if (Input.GetKey(KeyCode.LeftAlt))
        {
            float zoomInput = Input.GetAxis("Mouse Y");
            float newSize = Camera.main.orthographicSize - zoomInput * zoomSpeed * Time.deltaTime;
            newSize = Mathf.Clamp(newSize, minZoom, maxZoom);
            Camera.main.orthographicSize = newSize;
        }
    }
}