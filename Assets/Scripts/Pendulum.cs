using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendulum : MonoBehaviour
{
    private Rigidbody2D rb;
    private DistanceJoint2D joint;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        joint = GetComponent<DistanceJoint2D>();
    }

    void Update()
    {
        // Calcular la posici�n del extremo del p�ndulo
        Vector2 pendulumEnd = rb.position + joint.anchor;

        // Lanzar un rayo desde el objeto hacia el extremo del p�ndulo
        RaycastHit2D hit = Physics2D.Raycast(rb.position, pendulumEnd - rb.position, joint.distance);

        // Si el rayo golpea otro objeto
        if (hit.collider != null)
        {
            // Realizar acciones en respuesta a la colisi�n, por ejemplo:
            Debug.Log("P�ndulo golpe� un objeto: " + hit.collider.gameObject.name);

            // Puedes realizar acciones adicionales aqu�, como desactivar el DistanceJoint2D
            // joint.enabled = false;
        }
    }
}
