using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Tilemaps;
using UnityEngine.Windows;
using static SaveData;

public class SaveDataManager : MonoBehaviour
{
    private const string saveFileName = "savegame.json";

    [SerializeField] private GameObject _TeamA;
    [SerializeField] private GameObject _TeamB;

    [SerializeField] private GameObject _WormPrefab;

    [SerializeField] private GameObject TurnManager;

    [SerializeField] private Tilemap tilemap;

    public void SaveData()
    {
        SaveData data = new SaveData();

        SaveWormsByTeam(data, 0);
        SaveWormsByTeam(data, 1);

        ISaveableTilemap saveableTilemap = tilemap.GetComponent<ISaveableTilemap>();
        data.PopulateTileamap(saveableTilemap);
        string jsonData = JsonUtility.ToJson(data);

        try
        {
            Debug.Log("Saving: ");
            Debug.Log(jsonData);

            System.IO.File.WriteAllText(saveFileName, jsonData);
        }
        catch (Exception e)
        {
            Debug.LogError($"Error while trying to save {Path.Combine(Application.persistentDataPath, saveFileName)} with exception {e}");
        }
    }

    private void SaveWormsByTeam(SaveData data, int teamID)
    {
        GameObject team;

        if (teamID == 0)
        {
            team = _TeamA;
            
        }
            
        else
        {
            team = _TeamB;
            
        }
            

        ISaveableObject[] saveableWorms = new ISaveableObject[team.transform.childCount];
        Debug.Log("Lenght: "+saveableWorms.Length);
        for (int i = 0; i < team.transform.childCount; i++)
        {
            saveableWorms[i] = team.transform.GetChild(i).GetComponent<ISaveableObject>();
            Debug.Log("SaveableWorm: "+saveableWorms[i]);
        }
        
        data.PopulateData(saveableWorms, teamID);
        string jsonData = JsonUtility.ToJson(data);
    }

    public void LoadData()
    {
        try
        {
            string jsonData = System.IO.File.ReadAllText(saveFileName);
            
            SaveData data = new SaveData();
            JsonUtility.FromJsonOverwrite(jsonData, data);

            LoadWormsByTeam(data, 0);
            LoadWormsByTeam(data, 1);

            TurnManager.GetComponent<TurnManager>().teamA = _TeamA;
            TurnManager.GetComponent<TurnManager>().teamB = _TeamB;

            TurnManager.GetComponent<TurnManager>().Load();
            tilemap.GetComponent<TileMapController>().Load(data.tileMap);
        }
        catch (Exception e)
        {
            Debug.LogError($"Error while trying to load {Path.Combine(Application.persistentDataPath, saveFileName)} with exception {e}");
        }
    }

    private void LoadWormsByTeam(SaveData data, int teamID)
    {
        GameObject team;
        WormData[] wormsData;
        if (teamID == 0)
        {
            team = _TeamA;
            wormsData = data.wormsTeamA;
            
        }
        else
        {
            team = _TeamB;
            wormsData = data.wormsTeamB;    
        }
        Debug.Log("team.transform.childCount :" + team.transform.childCount);
        for (int i = 0; i < team.transform.childCount; i++)
            Destroy(team.transform.GetChild(i).gameObject);

        foreach (SaveData.WormData wormData in wormsData)
        {
            GameObject go = Instantiate(_WormPrefab, team.transform);
            go.GetComponent<ISaveableObject>().Load(wormData);
        }
        

    }

    private void Update()
    {
        if (UnityEngine.Input.GetKeyDown(KeyCode.S))
            SaveData();

        if (UnityEngine.Input.GetKeyDown(KeyCode.L))
            LoadData();
    }
}