using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SaveData;

[Serializable]
public class SaveData
{
    [Serializable]
    public struct WormData
    {

        public Vector3 position;
        public float health;
        public float shield;
        public bool mover;
        public WormData(Vector3 _position, float _health, float _shield, bool _mover)
        {
            position = _position;
            health = _health;
            shield = _shield;
            mover = _mover;
        }
    }

    public WormData[] wormsTeamA;
    public WormData[] wormsTeamB;

    public TileMapData tileMap;

    [Serializable]
    public struct TileMapData
    {

        public List<Vector3> positions;

        public TileMapData(List<Vector3> _positions)
        {
            positions = _positions;
        }
    }


    public void PopulateData(ISaveableObject[] wormsData, int team)
    {
        if (team == 0)
            PopulateTeamData(wormsData, out wormsTeamA);
        else
        {
            PopulateTeamData(wormsData, out wormsTeamB);
        }
    }

    private void PopulateTeamData(ISaveableObject[] m_WormsData, out WormData[] team)
    {
        team = new WormData[m_WormsData.Length];
        for (int i = 0; i < m_WormsData.Length; i++)
            team[i] = m_WormsData[i].Save();
    }

    public void PopulateTileamap(ISaveableTilemap tilemapData)
    {
        tileMap = tilemapData.Save();
    }
}

public interface ISaveableObject
{
    public WormData Save();
    public void Load(WormData m_Worms);
}

public interface ISaveableTilemap
{
    public TileMapData Save();
    public void Load(TileMapData m_Tileamap);
}

