using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Unity.Netcode;
using UnityEngine;

public class Prueba : NetworkBehaviour
{
    //Variable senzilla, la pot updatejar el client -> Aix� no s'hauria de fer "que fan trampes"
    NetworkVariable<Color> m_Color = new NetworkVariable<Color>(Color.white, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);


    // Aix� s� que seria el nou awake
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        m_Color.OnValueChanged += OnColorChanged;


        //Codi que nom�s s'executar� al servidor
        if (IsServer)
        {
            transform.position = new Vector3(Random.Range(-7f, 7f), Random.Range(-5f, 5f), 0);
        }

        //Codi que s'executar� nom�s si ets owner
        if (!IsOwner)
            return;
    }

    // Aix� podriem dir que seria el nou ondestroy (per� nom�s en si implica xarxa)
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();

        m_Color.OnValueChanged -= OnColorChanged;
    }

    //Sempre tindran aquest format amb oldValue i newValue
    private void OnColorChanged(Color previousValue, Color newValue)
    {
        GetComponent<SpriteRenderer>().color = newValue;
    }

    void Update()
    {
        //Aquest update nom�s per a qui li pertany
        if (!IsOwner)
            return;

        if (Input.GetKeyDown(KeyCode.Space))
            m_Color.Value = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    }
}
