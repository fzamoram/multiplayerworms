using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet_Controller : MonoBehaviour
{
    private Rigidbody2D m_rb;
    private SpriteRenderer m_spriteRenderer;
    [SerializeField]
    private GameEvent m_Event;

    [SerializeField]
    private GameObject m_hitbox;

    [SerializeField]
    private LayerMask m_explosionLayer;


    [SerializeField]
    private float radius;

    [SerializeField]
    private float damage;

    [SerializeField]
    private GameObject m_Worm;

    public float force;


    [SerializeField]
    private GameEventVector3 m_EventExplosion;
    private void Awake()
    {
        force = m_Worm.GetComponentInChildren<Arma_Controller>().force;
        m_rb = GetComponent<Rigidbody2D>();
        m_spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Init(Vector3 direction)
    {
        m_rb.velocity = direction;
    }

    private void Update()
    {
        transform.right = m_rb.velocity;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        /*
        RaycastHit2D[] worms;
        worms = Physics2D.CircleCastAll(collision.transform.position, radius, Vector2.up, radius, m_explosionLayer);
        foreach (RaycastHit2D worm in worms)
        {
            float distance = Vector2.Distance(transform.position, worm.point);
            float proximity = (radius - distance) / radius;
            Debug.Log("FUERZA: " + force);
            Debug.Log("DISTANCIA: " + distance);
            Debug.Log("PROXIMITY" + proximity);
            worm.transform.GetComponent<Worm_Controller_States>().DamageImpact(damage, force, proximity);
        }
        Destroy(gameObject);
        */
    }

    /*
    private void OnCollisionEnter2D(Collision2D collision)
    {
            m_spriteRenderer.color = Color.clear;
    }
    */
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Limit")
        {
            Destroy(gameObject);
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {

        RaycastHit2D[] worms;
        worms = Physics2D.CircleCastAll(collision.contacts[0].point, radius, Vector2.up, radius, m_explosionLayer);
        foreach (RaycastHit2D worm in worms)
        {   
            float distance = Vector2.Distance(transform.position, worm.point);
            float proximity = Mathf.Abs((radius - distance)) / radius;
            Debug.Log("FUERZA: " + force);
            Debug.Log("DISTANCIA: " + distance);
            Debug.Log("PROXIMITY" + proximity);
            worm.transform.GetComponent<Worm_Controller_States>().DamageImpact(damage,force,proximity);
        }
        
        m_EventExplosion.Raise(radius,collision.contacts[0].point);
        Destroy(gameObject);
    }


    private void OnDestroy()
    {
        m_Event.Raise();

    }
}
