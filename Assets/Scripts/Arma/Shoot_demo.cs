using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Accessibility;

public class Shoot_demo : MonoBehaviour
{

    private GameObject misil;
    Arma_Controller arma;

    [SerializeField]
    GameEvent shoot;

    private int disparos = 1;


    [SerializeField]
    private Transform shootPoint;

    void Start()
    {
 
        arma = GetComponent<Arma_Controller>();
        //disparos = gameObject.GetComponent<Arma_Controller>().gun.bullets;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Shot();
        }
    }

    private void Shot()
    {
        if (disparos != 0)
        {
            misil = gameObject.GetComponent<Arma_Controller>().gun.Ammo;
            GameObject _misil = Instantiate(misil, shootPoint.position, transform.rotation);
            _misil.GetComponent<Rigidbody2D>().velocity = transform.right * arma.force;
            _misil.GetComponent<Bullet_Controller>().force = arma.force;
            disparos--;
            //if (disparos == 0)
            shoot.Raise();
        }
    }

    public void ResetShoots()
    {
        disparos = 1;
    }

}
