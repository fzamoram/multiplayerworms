using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma_Controller : MonoBehaviour
{
    public Vector2 Direction;

    public float force;
    private bool move = true;
    public GameObject point;
    public GameObject[] Points;
    public int numberofPoints;
    public List<Items_SO> guns;


    public Items_SO gun;

    public int currentGunIndex = 0;


    


    private void Start()
    {
        //ChangeGun(0);
    
            Points = new GameObject[numberofPoints];

            for (int i = 0; i < numberofPoints; i++)
            {
                Points[i] = Instantiate(point, transform.position, Quaternion.identity);
                Points[i].transform.parent = transform;
            }
  

        //this.GetComponent<Rigidbody>().velocity = new Vector2(-3f, 0);
        
    }


    private void Update()
    {
        if (move)
        {
     
            force -= Input.mouseScrollDelta.y;

            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Vector2 misilPosition = transform.position;

            Direction = mousePosition - misilPosition;

            faceMouse();

            for (int i = 0; i < numberofPoints; i++)
            {
                Points[i].transform.position = PointPosition(i * 0.1f);
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (currentGunIndex > 0)
            {
                currentGunIndex--;
                ChangeGun(currentGunIndex);
            }

        }
        else if (Input.GetKeyDown(KeyCode.RightControl))
        {
            if (currentGunIndex < 10)
            {
                currentGunIndex++;
                ChangeGun(currentGunIndex);
            }

        }
    }


    void faceMouse()
    {
        transform.right = Direction;
        if (Direction.x < 0)
        {
            gameObject.GetComponent<SpriteRenderer>().flipY = true;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().flipY = false;
        }
    }

     Vector2 PointPosition(float t) {
        Vector2 pos = (Vector2)transform.position + (Direction.normalized * force * t) + 0.5f * Physics2D.gravity * (t*t);
        return pos;
    }

    public void StopPoints()
    {
        move = false;
    }

    public void MovePoints()
    {
        move = true;
    }



    public void ChangeGun(int gunIndex)
    {
        if (gunIndex >= 0 && gunIndex < guns.Count)
        {
                 gun = guns[gunIndex];

            if (gun != null && gun.Sprite != null)
            {
                gameObject.GetComponent<SpriteRenderer>().sprite = gun.Sprite;
                currentGunIndex = gunIndex;
            }

        }
    }



    public void demo(Items_SO gunSO)
    {
        gun = gunSO;
        gameObject.GetComponent<SpriteRenderer>().sprite = gunSO.Sprite;
    }
}
