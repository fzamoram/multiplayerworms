using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "TilemapData", menuName = "ScriptableObjects/TilemapData")]
public class TilemapData : ScriptableObject
{
    public Vector3Int[] tilePositions;
}
