using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "ScriptableObjects/Guns/Misil_Info")]
public class Items_SO : ScriptableObject
{
    [SerializeField]
    protected Sprite m_Sprite = null;
    public Sprite Sprite { get => m_Sprite; set => m_Sprite = value; }

    [SerializeField]
    protected GameObject m_ammo = null;
    public  GameObject Ammo { get => m_ammo; set => m_ammo = value; }

    [SerializeField]
    protected float _force = 0;
    public float force { get => _force; set => _force = value; }

    [SerializeField]
    protected int _bullets = 0;
    public int bullets { get => _bullets; set => _bullets = value; }

}
