using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Consumables/Consumable_Info")]
public class Consumables_SO : ScriptableObject
{

    [SerializeField]
    protected float m_jumpForce = 0;
    public float jumpForce { get => m_jumpForce; set => m_jumpForce = value; }

    [SerializeField]
    protected float m_velocity = 0;
    public float velocity { get => m_velocity; set => m_velocity = value; }

    [SerializeField]
    protected float m_health = 0;
    public float health { get => m_health; set => m_health = value; }


    [SerializeField]
    protected float m_shield= 0;
    public float shield { get => m_shield; set => m_shield = value; }



}
