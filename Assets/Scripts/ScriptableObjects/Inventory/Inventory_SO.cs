using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Inventory")]
public class Inventory_SO : ScriptableObject
{
    [SerializeField]
    protected float m_Bazooka = 0;
    public float num_Bazooka { get => m_Bazooka; set => m_Bazooka = value; }
   
    [SerializeField]
    protected float m_Pistol = 0;
    public float num_Pistol { get => m_Pistol; set => m_Pistol = value; }

}
