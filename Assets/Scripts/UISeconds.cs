using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UISeconds : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI textMeshProUGUI;
    
    public void UpdateSeconds(int sec)
    {
        textMeshProUGUI.text = "Seconds: " + sec;
    }
}
