using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory_button : MonoBehaviour
{
    [SerializeField]
    private GameObject Inventory;

    [SerializeField]
    private GameObject close_button;


    [SerializeField]
    private GameObject inv_button;





    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void ShowInventory()
    {
        Inventory.SetActive(true);
        close_button.SetActive(true);
        inv_button.SetActive(false);
    }

    public void CloseInventory()
    {
        Inventory.SetActive(false);
        close_button.SetActive(false);
        inv_button.SetActive(true);
    }


}
