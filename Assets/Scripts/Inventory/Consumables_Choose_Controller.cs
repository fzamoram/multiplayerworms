using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumables_Choose_Controller : MonoBehaviour
{
    [SerializeField]
    private GameEventConsumable gameEvent;

    [SerializeField]
    private Consumables_SO consumable;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PickConsumble()
    {
        gameEvent.Raise(consumable);
    }
}
