using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory_Choose_Controller : MonoBehaviour
{
    [SerializeField]
    private GameEventGun gameEvent;

    [SerializeField]
    private Items_SO gun;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeGun()
    {
        gameEvent.Raise(gun);
    }
}
