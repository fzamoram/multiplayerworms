using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Team_Creator : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> worms;

    [SerializeField]
    private GameObject wormPrefab;

    void Awake()
    {
        worms = new List<GameObject>();
    }

    public void MakeWorms(int numberOfWorms)
    {
        for (int i = 0; i < numberOfWorms; i++)
        {
            GameObject newWorm = Instantiate(wormPrefab, transform.position, Quaternion.identity);
            worms.Add(newWorm);
        }
    }

}
