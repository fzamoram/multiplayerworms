using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.WSA;
using static UnityEngine.Rendering.DebugUI;

public class TileMapController : MonoBehaviour, ISaveableTilemap
{
    
    Tilemap tilemap;
    public TilemapData tilemapData;
    public TileBase tile;

    void Awake()
    {
        tilemap = GetComponent<Tilemap>();
        //LoadTilemap();

    }

    public void Destruction(float radius_explosion, Vector3 coordenadesWorld)
    {
        Debug.Log("ENTRA A DESTRUIR");
        Vector3Int coordenadesTilemap = tilemap.WorldToCell(coordenadesWorld);

        if (radius_explosion >= 2.0f)
        {
            for (int x = -Mathf.FloorToInt(radius_explosion); x <= Mathf.FloorToInt(radius_explosion); x++)
            {
                for (int y = -Mathf.FloorToInt(radius_explosion); y <= Mathf.FloorToInt(radius_explosion); y++)
                {
                    Vector3Int cellPosition = coordenadesTilemap + new Vector3Int(x, y, 0);

                    if (Vector3.Distance(tilemap.GetCellCenterWorld(cellPosition), coordenadesTilemap) <= radius_explosion)
                    {
                        tilemap.SetTile(cellPosition, null);
                    }
                }
            }
        }
        else
        {
            // Forma de "+"
            for (int i = -Mathf.FloorToInt(radius_explosion); i <= Mathf.FloorToInt(radius_explosion); i++)
            {
                Vector3Int horizontalPosition = coordenadesTilemap + new Vector3Int(i, 0, 0);
                Vector3Int verticalPosition = coordenadesTilemap + new Vector3Int(0, i, 0);

                tilemap.SetTile(horizontalPosition, null); // Borra la celda horizontal
                tilemap.SetTile(verticalPosition, null); // Borra la celda vertical
            }
        }
    }


    public void Load(SaveData.TileMapData _TilemapData)
    {
        DeleteTiles();
        foreach (Vector3 worldPosition in _TilemapData.positions)
        {
            // Convierte la posici�n del mundo a la posici�n de la celda en el Tilemap
            Vector3Int cellPosition = tilemap.WorldToCell(worldPosition);

            // Establece el tile en la posici�n de la celda actual usando el tile especificado
            tilemap.SetTile(cellPosition, tile);
        }
    }

    public SaveData.TileMapData Save()
    {
        return new SaveData.TileMapData(GetTilesPositions());
    }

    private List<Vector3> GetTilesPositions()
    {
        BoundsInt bounds = tilemap.cellBounds;
        List<Vector3> tilesPositions = new List<Vector3>();

        foreach (Vector3Int cellPosition in bounds.allPositionsWithin)
        {
            
            Vector3 tilePosition = tilemap.GetCellCenterWorld(cellPosition);
            if (tilemap.GetTile(cellPosition) != null)
            tilesPositions.Add(tilePosition);
        }
        return tilesPositions;
    }

    private void DeleteTiles()
    {
        BoundsInt bounds = tilemap.cellBounds;
        foreach (Vector3Int cellPosition in bounds.allPositionsWithin)
        {
            tilemap.SetTile(cellPosition, null);
        }
    }
}



