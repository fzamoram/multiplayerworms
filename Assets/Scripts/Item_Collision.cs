using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Collision : MonoBehaviour
{
    public GameEventConsumable pick;
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Worm")
        {
            pick.Raise(gameObject.GetComponent<ItemType>().consumable);
            Destroy(gameObject);
        }
    }
}
