using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

public class Worm_Controller_States : MonoBehaviour, ISaveableObject
{
    [SerializeField]
    GameEvent changeHealth;

    [SerializeField]
    private InputActionAsset m_Input;
    private InputAction m_movement;
    public float speed = 3f;
    public float jumpForce = 3f;
    public float jetpackForce = 10f;
    private Vector2 moveInput;
    private Rigidbody2D rb;

    [SerializeField]
    private Animator m_Animator;

    public float max_Health;
    public float health;
    public float max_Shield;
    public float shield;
    private int saltos = 1;
    private bool isFliying = false;

    public bool mover;

    [SerializeField]
    GameObject m_Gun;
    /*
    [SerializeField]
    private HitboxInfo m_HitboxInfo;
    */

    [SerializeField]
    private GameEventWorm m_Event;

    [SerializeField]
    private GameEvent m_MovePoints;


   

    [SerializeField]
    private GameObject point_Mark;

    [SerializeField]
    private GameObject inv_button;


    [SerializeField]
    private Dictionary<Consumables_SO, int> consumables_inv;


    private float defaultJumpForce = 5f;

    private float defaultSpeed = 3f;



    


    private enum SwitchMachineStates { NONE, IDLE, WALK, JUMP, HIT2, HURT };
    private SwitchMachineStates m_CurrentState;

    //canviarem d'estat sempre mitjanant aquesta funci
    private void ChangeState(SwitchMachineStates newState)
    {
        //no caldria fer-ho, per evitem que un estat entri a si mateix
        //s possible que la nostra mquina ho permeti, per tant aix
        //no es faria sempre.
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                rb.velocity = Vector2.zero;
                m_Animator.Play("idle");

                break;

            case SwitchMachineStates.WALK:

                m_Animator.Play("walk");

                break;


            case SwitchMachineStates.JUMP:

                m_Animator.Play("jump");

                break;

            case SwitchMachineStates.HIT2:

                m_Animator.Play("hit2");

                break;

            case SwitchMachineStates.HURT:

                m_Animator.Play("hurt");

                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.JUMP:

                m_ComboAvailable = false;

                break;

            case SwitchMachineStates.HIT2:

                m_ComboAvailable = false;

                break;
            case SwitchMachineStates.HURT:



                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                if ((m_movement.ReadValue<Vector2>().x != 0 || m_movement.ReadValue<Vector2>().y != 0) && mover)
                    ChangeState(SwitchMachineStates.WALK);

                break;
            case SwitchMachineStates.WALK:
                if (mover)
                {
                    moveInput.x = m_movement.ReadValue<Vector2>().x;
                    rb.velocity = new Vector2(moveInput.x * speed, rb.velocity.y);
                }

                Health_Controller[] healthControllers = GetComponentsInChildren<Health_Controller>();


                if (rb.velocity.x < 0)
                {

                    foreach (Health_Controller healthController in healthControllers)
                    {
                        transform.rotation = Quaternion.Euler(0f, 180f, 0f);
                       healthController.barra_Vida.fillOrigin = 1;
                    }

                }

                else if (rb.velocity.x > 0)
                {

                    foreach (Health_Controller healthController in healthControllers)
                    {
                        transform.rotation = Quaternion.identity;
                        healthController.barra_Vida.fillOrigin = 0;
                    }

                }
                if (rb.velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);
                break;
            case SwitchMachineStates.JUMP:
                m_ComboAvailable = true;
                break;

            case SwitchMachineStates.HIT2:
                m_ComboAvailable = false;
                break;

            case SwitchMachineStates.HURT:
                break;
            default:
                break;
        }
    }

    //------------------------------------------------------------------------//
    //Possible implementaci del sistema de combo
    private bool m_ComboAvailable = false;

    public void InitComboWindow()
    {
        m_ComboAvailable = true;
    }

    public void EndComboWindow()
    {
        m_ComboAvailable = false;
    }

    public void EndHit()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    void Awake()
    {
        max_Health = 10;
        health = max_Health;
        inv_button.SetActive(false);
        rb = GetComponent<Rigidbody2D>();
        m_Input = Instantiate(m_Input);
        m_Input.FindActionMap("Movement").FindAction("Move").started += OnMove;
        m_Input.FindActionMap("Movement").FindAction("Move").performed += OnMove;
        m_Input.FindActionMap("Movement").FindAction("Jump").performed += Jump;
        m_Input.FindActionMap("Movement").FindAction("Jetpack").performed += Jetpack;
        m_Input.FindActionMap("Movement").FindAction("Shoot").performed += Shoot;
        //m_Input.FindActionMap("Movement").FindAction("Punch").performed += AttackAction;
        m_Input.FindActionMap("Movement").FindAction("Move").canceled += OnCancelMove;
        m_movement = m_Input.FindActionMap("Movement").FindAction("Move");
        m_Input.FindActionMap("Movement").Enable();
        m_Animator = GetComponent<Animator>();
    }

    void Start()
    {

        InitState(SwitchMachineStates.IDLE);

    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();

    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(moveInput.x * speed, rb.velocity.y);
    }

    private void OnMove(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.IDLE);
    }

    private void OnCancelMove(InputAction.CallbackContext context)
    {
        moveInput = Vector2.zero;
    }

    private void Jump(InputAction.CallbackContext context)
    {
        if (saltos == 1 && mover)
        {
            saltos--;
            rb.AddForce(new Vector2(moveInput.x, jumpForce), ForceMode2D.Impulse);
            ChangeState(SwitchMachineStates.JUMP); 
        }
    }


    private void Jetpack(InputAction.CallbackContext context)
    {
        //this.rb.AddForce(Vector2.up * jetpackForce, ForceMode2D.Impulse);

    }

    private void Shoot(InputAction.CallbackContext context)
    {
        if (mover)
        {
            mover = false;
        }
       
    }
    private void OncancelJump()
    {

    }

    private void OnDestroy()
    {
        m_Input.FindActionMap("Movement").FindAction("Move").started -= OnMove;
        m_Input.FindActionMap("Movement").FindAction("Move").performed -= OnMove;
        m_Input.FindActionMap("Movement").FindAction("Move").canceled -= OnCancelMove;
        m_Input.FindActionMap("Movement").FindAction("Shoot").performed -= Shoot;
        //m_Input.FindActionMap("Movement").FindAction("Punch").performed -= AttackAction;
        m_Input.FindActionMap("Movement").Disable();
        m_Event.Raise(this);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

    }

    private void OnTriggerStay2D(Collider2D collision)
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Suelo")
        {
            if (saltos == 0)
            {
                saltos++;
                ChangeState(SwitchMachineStates.IDLE);
            }
            
        }
    }

    public void EmpiezaTurno()
    {
        mover = true;
        speed = defaultSpeed;
        jumpForce = defaultJumpForce;
        inv_button.SetActive(true);
        Debug.Log(m_Gun);
        m_Gun.SetActive(true);
        point_Mark.SetActive(true);
        m_Gun.GetComponent<Shoot_demo>().ResetShoots();
        m_MovePoints.Raise();
    }
    public void TerminaTurno()
    {
        inv_button.SetActive(false);
        point_Mark.SetActive(false);
        mover = false;
        Debug.Log(m_Gun);
        m_Gun.SetActive(false);
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Limit")
        {
            Destroy(gameObject);
        }

    }

    public void DamageImpact(float misil_damage, float misil_force, float proximity)
    {
        if (shield <= 0)
        {
            float dmg = ((misil_damage * misil_force * proximity)*100 / 2);
            health -= dmg;
            
            changeHealth.Raise();
        }
        else {
            float dmg = ((misil_damage * misil_force * proximity) * 100 / 2);
            shield -= dmg;
            if (dmg - shield > 0)
            {
                health -= (dmg - shield);
            }
            changeHealth.Raise();
        }
        ChangeState(SwitchMachineStates.HURT);
        Debug.Log(((misil_damage * misil_force * proximity)*100 / 2));

    }



    public void hitToIdle()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }


    public void Consumable(Consumables_SO consumable)
    {
        
                    speed = consumable.velocity;
                    health += consumable.health;
                    shield += consumable.shield;
                    jumpForce = consumable.jumpForce;
                    changeHealth.Raise();
                    gameObject.GetComponent<Item_Controller>().numItems[consumable]--;

    }


    public void PickConsumable(Consumables_SO consumable)
    {
        if (mover) {
            var itemController = gameObject.GetComponent<Item_Controller>();

            var consumableKeys = new List<Consumables_SO>(itemController.numItems.Keys);

            foreach (var consum in consumableKeys)
            {
                if (consum == consumable)
                {
                    itemController.numItems[consum]++;
                    print(consum + " " + itemController.numItems[consum]);
                }
            }
        }
        /*
        foreach (var consum in gameObject.GetComponent<Item_Controller>().numItems)
        {
            if (consum.Key == consumable)
            {
                gameObject.GetComponent<Item_Controller>().numItems[consum.Key]++;
                print(consum.Key + " " + consum.Value);
            }
        }
        */
    }

    public void Load(SaveData.WormData _wormData)
    {
        Debug.Log("------------------------------------------");
        Debug.Log("Position: " + _wormData.position);
        Debug.Log("Health: " + _wormData.health);
        Debug.Log("Defense: " + _wormData.shield);
        Debug.Log("Mover: " + _wormData.mover);
        transform.position = _wormData.position;
        health = _wormData.health;
        shield = _wormData.shield;
        mover = _wormData.mover;
        if (!mover)
            m_Gun.SetActive(false);
    }

    public SaveData.WormData Save()
    {
        return new SaveData.WormData(transform.position, health, shield, mover);
    }

}


