using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - Gun")]
public class GameEventGun : GameEvent<Items_SO>{}
