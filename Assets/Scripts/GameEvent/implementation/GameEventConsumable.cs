using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - Consumable")]
public class GameEventConsumable : GameEvent<Consumables_SO> { }

