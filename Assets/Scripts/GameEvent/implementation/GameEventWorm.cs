using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - Worm")]
public class GameEventWorm : GameEvent<Worm_Controller_States> { }
