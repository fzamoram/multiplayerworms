using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Spawn_Teams_Controller : MonoBehaviour
{
    public GameObject TeamA;
    public GameObject TeamB;
    public Tilemap tilemap;

    private void Start()
    {
        // Llama a MakeWorms en Team_Creator
        Team_Creator teamCreator = GetComponentInChildren<Team_Creator>();
        if (teamCreator != null)
        {
            teamCreator.MakeWorms(2);
        }

        // Llama a SpawnObjectOnTilemap
        SpawnObjectOnTilemap();
    }

    void SpawnObjectOnTilemap()
    {
        BoundsInt bounds = tilemap.cellBounds;

        Vector3Int randomPosition = new Vector3Int(
            Random.Range(bounds.x, bounds.x + bounds.size.x),
            Random.Range(bounds.y, bounds.y + bounds.size.y),
            0
        );

        Vector3Int randomPosition2 = new Vector3Int(
            Random.Range(bounds.x, bounds.x + bounds.size.x),
            Random.Range(bounds.y, bounds.y + bounds.size.y),
            0
        );

        Vector3 spawnPosition = tilemap.GetCellCenterWorld(randomPosition);
        Vector3 spawnPosition2 = tilemap.GetCellCenterWorld(randomPosition2);

        Instantiate(TeamA, spawnPosition, Quaternion.identity);
        Instantiate(TeamB, spawnPosition2, Quaternion.identity);
    }
}
